import { Component, OnInit } from '@angular/core'
import { ConfigService } from '../_demo-core/config.service'
import { DemoConfig } from 'src/app/_demo-core/_demo-core'
import { Router } from '@angular/router'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public config: DemoConfig
  public configured: boolean = false

  constructor(
    private configService: ConfigService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    if (this.configService.authToken.value) {
      this.router.navigate(['guarded-route'])
    }
    this.config = this.configService.config
    this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
  }
}
