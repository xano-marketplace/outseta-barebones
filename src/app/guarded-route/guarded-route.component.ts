import { Component, Inject, OnInit } from '@angular/core'
import { ConfigService } from 'src/app/_demo-core/config.service'
import { MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'app-guarded-route',
  templateUrl: './guarded-route.component.html',
  styleUrls: ['./guarded-route.component.scss'],
})
export class GuardedRouteComponent implements OnInit {
  public outsetaProfile: any
  public extras: any
  public editId: number
  public todos: any[] = []

  constructor(
    private configService: ConfigService,
  ) {
  }

  ngOnInit(): void {
    this.configService.xanoAPI('/outseta/auth_demo', 'get', null, {}, true)
      .subscribe(
        res => this.extras = res,
        error => this.configService.showErrorSnack(error),
      )

    this.configService.xanoAPI('/outseta/profile', 'get', null, {}, true)
      .subscribe(
        res => this.outsetaProfile = res,
        error => this.configService.showErrorSnack(error),
      )

    this.configService.xanoAPI('/outseta/todo', 'get', null, {}, true)
      .subscribe(
        res => this.todos = res,
        error => this.configService.showErrorSnack(error),
      )
  }


  public save(todo?, remove?): void {
    if (!todo) {
      todo = {
        task: 'New task',
      }
    }
    this.configService.xanoAPI('/outseta/todo', 'post', { ...todo, delete: remove }, {}, true)
      .subscribe(
        res => {
          if (res) {
            if (res.deleted) {
              this.todos = this.todos.filter(x => x.id !== res.id)
            } else if (!todo?.id) {
              this.todos.push(res)
            } else {
              const index = this.todos.findIndex(x => x.id === res.id)
              if (index > -1) this.todos[index] = res
            }
          }
        },
        error => this.configService.showErrorSnack(error),
      )
  }
}
