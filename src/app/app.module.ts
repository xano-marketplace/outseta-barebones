import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { HeaderComponent } from './header/header.component'
import { HomeComponent } from './home/home.component'
import { HttpClientModule } from '@angular/common/http'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatButtonModule } from '@angular/material/button'
import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatDialogModule } from '@angular/material/dialog'
import { ReactiveFormsModule } from '@angular/forms'
import { MatIconModule } from '@angular/material/icon'
import { MAT_SNACK_BAR_DEFAULT_OPTIONS, MatSnackBarModule } from '@angular/material/snack-bar'
import { MatCommonModule } from '@angular/material/core'
import { MatDividerModule } from '@angular/material/divider'
import { MatExpansionModule } from '@angular/material/expansion'
import { DemoCoreModule } from './_demo-core/demo-core.module'
import { MatMenuModule } from '@angular/material/menu'
import { GuardedRouteComponent } from './guarded-route/guarded-route.component'
import { OutsetaLoginComponent } from './outseta-login/outseta-login.component'
import { OutsetaRedirectUrlComponent } from './outseta-redirect-url/outseta-redirect-url.component'
import { MatCheckboxModule } from '@angular/material/checkbox'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    GuardedRouteComponent,
    OutsetaLoginComponent,
    OutsetaRedirectUrlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatCommonModule,
    HttpClientModule,
    DemoCoreModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,
    MatCardModule,
    MatInputModule,
    MatDialogModule,
    MatToolbarModule,
    MatDividerModule,
    MatExpansionModule,
    MatCheckboxModule,
  ],
  providers: [{
    provide: MAT_SNACK_BAR_DEFAULT_OPTIONS,
    useValue: {
      duration: 4000,
      horizontalPosition: 'start',
    },
  }],
  bootstrap: [AppComponent],
})
export class AppModule {
}
