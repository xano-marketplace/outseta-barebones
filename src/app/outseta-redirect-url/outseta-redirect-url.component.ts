import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ConfigService } from 'src/app/_demo-core/config.service'
import { mergeMap } from 'rxjs/operators'
import { EMPTY } from 'rxjs'

@Component({
  selector: 'app-outseta-redirect-url',
  templateUrl: './outseta-redirect-url.component.html',
  styleUrls: ['./outseta-redirect-url.component.scss'],
})
export class OutsetaRedirectUrlComponent implements OnInit {

  constructor(
    private configService: ConfigService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(
      mergeMap(params => {
        console.log(params?.access_token)
        if (params?.access_token) {
          return this.configService.xanoAPI('/outseta/auth', 'post', {
            token: params.access_token,
          })
        } else {
          return EMPTY
        }
      }),
    ).subscribe(res => {
      if (res?.authToken) {
        this.configService.authToken.next(res.authToken)
        this.router.navigate(['guarded-route'])
      } else {
        this.router.navigate(['home'])
      }

    }, error => this.configService.showErrorSnack(error))
  }

}
