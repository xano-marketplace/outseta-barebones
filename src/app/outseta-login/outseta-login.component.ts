import { AfterViewInit, Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core'
import { ConfigService } from 'src/app/_demo-core/config.service'

@Component({
  selector: 'app-outseta-login',
  templateUrl: './outseta-login.component.html',
  styleUrls: ['./outseta-login.component.scss'],
})
export class OutsetaLoginComponent implements OnInit, AfterViewInit {

  @ViewChild('outseta') outseta: ElementRef

  constructor(
    private configService: ConfigService,
    private renderer: Renderer2) {
  }

  ngOnInit(): void {

  }

  ngAfterViewInit() {
    this.configService.xanoAPI('/outseta/auth_widget_url', 'get')
      .subscribe(res => {
        const scriptElement = this.renderer.createElement('script')
        scriptElement.type = 'text/javascript'
        scriptElement.src = res
        this.renderer.appendChild(this.outseta.nativeElement, scriptElement)
      })
  }
}
