import { DemoConfig } from './_demo-core'

export const config: DemoConfig = {
  title: 'Outseta Login',
  summary: '',
  marketplace_type: 'extension',
  editLink: 'https://gitlab.com/xano-marketplace/outseta-barebones/',
  components: [
    {
      name: 'Home',
      description: 'This component contains the auth widget from Outseta that users can use to authenticate into your application.',
    },
    {
      name: 'Guarded Route',
      description: '' +
        'This component is only accessible if authenticated and will call the /outseta/auth_demo route to demonstrate the use ' +
        'of Outseta information stored in auth extras. Additionally, this route calls /outseta/profile endpoint and ' +
        'displays the email and name-based info provided from the Outseta Profile to demonstrate external calls to Outseta using your access token.' +
        'Additionally, we have included a todo list for a more robust demonstration of authenticating your app and endpoints with Outseta',
    },

  ],
  instructions: [
    'Install this extension in your desired workspace.',
    'You will need an <a href="https://www.outseta.com/" target="_blank">Outseta Account</a>',
    'In your Outseta dashboard, go to <b>Auth –> Sign up and Login</b> and setup a team mode auth with a post login url: https://demo.xano.com/outseta-barebones/auth-outseta, ' +
    'Next, <b>Show advanced options</b> for Login settings. Generate a new key, copy this key, and set it as your' +
    ' <b>outseta_jwt_key</b> Xano environment variable by clicking <b>Configure</b> on the extension page. (Only copy the contents between the begin and end certificate markers).',
    'You will also need to create your Outseta auth widget, which can be done in your Outseta dashboard under <b>Auth –> Embeds.</b>',
    'For detailed information on setting up the Outseta auth widget, see <a href="https://go.outseta.com/support/kb#/articles/By9q2qWA/how-to-integrate-outseta-s-sign-up-login-and-profile-widgets" target="_blank">official Outseta documentation.</a>',
    'You will also need to set up your <b>outseta_url</b> in your Xano environment variables by clicking <b>Configure</b> on the extension page. You can find this in Outseta by going to <b>Settings -> General</b>. (the form should be: https://your_organization_name.outseta.com).',
    'Go to the newly added Outseta API Group and copy your API BASE URL',
    'Finally, in this demo, paste this API BASE URL in as "Your Xano API URL"',
  ],
  requiredApiPaths: [
    '/outseta/auth',
    '/outseta/todo',
    '/outseta/auth_demo',
    '/outseta/profile',
    '/outseta/auth_widget_url',
  ],
}

