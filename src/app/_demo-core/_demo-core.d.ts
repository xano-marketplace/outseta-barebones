export interface DemoConfig {
  title: string;
  summary: string;
  editLink: string;
  components: any;
  instructions: any;
  marketplace_type: string;
  requiredApiPaths: string[];
}

export interface Arg {
  tag?: string;
  value?: any;
}

export interface Filters {
  name?: string;
  arg?: Arg[];
}

export interface Left {
  operand: string;
  tag: string;
  filters?: Filters[];
}

export interface Right {
  operand: string;
  tag: string;
  filters?: Filters[];
  ignore_empty?: boolean;
}

export interface Expression {
  operator?: string;
  left: Left;
  right?: Right;
  or?: boolean;
}

export interface ColumnSearchExpression {
  operator?: string;
  column: string;
  query: any;
  or?: boolean;
}

export interface GeoSearchExpression {
  column: string;
  lng: number;
  lat: number;
  radius: number;
  or?: boolean;
}
