import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { DemoLandingComponent } from './demo-landing/demo-landing.component'
import { SafeHtmlPipe } from './safe-html.pipe'
import { MatFormFieldModule } from '@angular/material/form-field'
import { ReactiveFormsModule } from '@angular/forms'
import { MatDividerModule } from '@angular/material/divider'
import { MatButtonModule } from '@angular/material/button'
import { MatExpansionModule } from '@angular/material/expansion'
import { MatIconModule } from '@angular/material/icon'
import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatCommonModule } from '@angular/material/core'
import { FormGeneratorComponent } from './form-generator/form-generator.component'
import { ImageUploadComponent } from './image-upload/image-upload.component'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'

@NgModule({
  declarations: [
    DemoLandingComponent,
    SafeHtmlPipe,
    FormGeneratorComponent,
    ImageUploadComponent,
  ],
  exports: [
    FormGeneratorComponent,
    ImageUploadComponent,
  ],
  imports: [
    CommonModule,
    MatCommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatButtonModule,
    MatExpansionModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatProgressSpinnerModule,
  ],
})
export class DemoCoreModule {
}
