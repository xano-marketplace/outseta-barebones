import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  constructor(
    private http: HttpClient,
  ) {
  }

  get({
    endpoint,
    params,
    headers,
    responseType,
    stateFunc,
  }: {
    endpoint: string,
    responseType?: string,
    params?: {},
    headers?: {},
    stateFunc?: (item: boolean) => void
  }) {
    return this.req({ ...arguments[0], method: 'GET' })
  }

  post({
    endpoint,
    params,
    headers,
    responseType,
    stateFunc,
  }: {
    endpoint: string,
    responseType?: string,
    params?: {},
    headers?: {},
    stateFunc?: (item: boolean) => void
  }) {
    return this.req({ ...arguments[0], method: 'POST' })
  }

  delete({
    endpoint,
    params,
    headers,
    responseType,
    stateFunc,
  }: {
    endpoint: string,
    responseType?: string,
    params?: {},
    headers?: {},
    stateFunc?: (item: boolean) => void
  }) {
    return this.req({ ...arguments[0], method: 'DELETE' })
  }

  private req({
    endpoint,
    method,
    params = {},
    headers = {},
    responseType = 'json',
    stateFunc,
  }: {
    endpoint: string,
    method: string,
    responseType?: string,
    params?: {},
    headers?: {},
    stateFunc?: (item: boolean) => void
  }) {
    let options = {}

    if (stateFunc) {
      stateFunc(true)
    }

    if (['GET', 'DELETE'].includes(method) && params) {
      let httpParams = new HttpParams()
      for (let i in params) {
        if (typeof params[i] == 'object') {
          httpParams = httpParams.set(i, JSON.stringify(params[i]))
        } else {
          httpParams = httpParams.set(i, params[i])
        }
      }
      options['params'] = httpParams
    }

    let httpHeaders = new HttpHeaders

    if (headers) {
      Object.keys(headers).forEach(key => {
        httpHeaders = httpHeaders.set(key, headers[key])
      })
    }

    options['headers'] = httpHeaders
    options['responseType'] = responseType

    let observable
    switch (method) {
      case 'GET':
        observable = this.http.get(endpoint, options)
        break
      case 'POST':
        observable = this.http.post(endpoint, params, options)
        break
      case 'DELETE':
        observable = this.http.delete(endpoint, options)
        break
    }

    return new Observable(newObserver => {
      observable.subscribe(
        item => {
          newObserver.next(item)
          newObserver.complete()
        },
        err => {
          newObserver.error(err)
        },
      ).add(() => {
        if (stateFunc) {
          stateFunc(false)
        }
      })
    })
  }
}
