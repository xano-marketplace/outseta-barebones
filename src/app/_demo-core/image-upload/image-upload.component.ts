import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core'

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.scss'],
})
export class ImageUploadComponent implements OnInit {

  public uploading: boolean
  @Input() image: any
  @Output() delete = new EventEmitter<boolean>()
  @Output() formData = new EventEmitter<FormData>()

  constructor() {
  }

  ngOnInit(): void {
  }

  public upload(event): void {
    this.uploading = true
    const file: File = event.target.files[0]
    const formData: FormData = new FormData()
    formData.append('content', file, file.name)

    this.formData.emit(formData)

    setTimeout(() => {
      const reader = new FileReader()
      reader.onload = () => this.image = reader.result
      reader.readAsDataURL(file)
      this.uploading = false
    }, 1000)
  }

  public deleteImage(): void {
    this.image = null
    this.delete.emit(true)
  }
}
